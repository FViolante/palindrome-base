package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular( ) {
		boolean result = Palindrome.isPalindrome("racecar");
		assertTrue("Test Failed", result);
	}

	@Test
	public void testIsPalindromeNegative( ) {
		boolean result = Palindrome.isPalindrome("this is not palindrome");
		assertFalse("Test Failed", result);
	}
	
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean result = Palindrome.isPalindrome("taco cat");
		assertTrue("Test Failed", result);
	}
	
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean result = Palindrome.isPalindrome("racecart");
		assertFalse("Test Failed", result);
	}	
	
}
